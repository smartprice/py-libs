from setuptools import setup, find_packages


setup(
    name='sppylibs',
    version='1.0.0',
    author='SmartPrice',
    author_email='it@smartprice.ru',
    description='Company-wide python libs',
    url='https://gitlab.com/smartprice/py-libs',
    packages=find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
    ],

    python_requires='>=3.6, <3.8',
    install_requires=[
        'requests ==2.18.1',
        'aiohttp ==2.2.5',
    ],
)
