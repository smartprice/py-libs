import sppylibs.common.http as http
from urllib.parse import quote, urlencode
from sppylibs.netconf import NETCONF_SRV
from base64 import b64decode
from sppylibs.common.async_wrapper import run_coroutine
import json


DEFAULT_RETRIES = 10
DEFAULT_NETCONF_SERVICE = 'consul/cl/http'
DEFAULT_COUNTER_NS = 'seq'


async def nextval_async(
    counter_name,
    *,
    retries=DEFAULT_RETRIES,
    timeout=None,
    pool=None,
    netconf_service=DEFAULT_NETCONF_SERVICE,
    counter_ns=DEFAULT_COUNTER_NS,
    seed_value=lambda: 0,
    req_logger=None,
):
    http_args = {}

    if pool is not None:
        http_args['pool'] = pool

    if timeout is not None:
        http_args['timeout'] = timeout

    if req_logger is not None:
        http_args['logger'] = req_logger

    consul = NETCONF_SRV[netconf_service]
    url = 'http://{}:{}/v1/kv/{}/{}'.format(consul['host'], consul['port'], quote(counter_ns), quote(counter_name))
    response = await http.get_async(url, **http_args)

    if response.status_code == 404:
        await http.put_async('{}?{}'.format(url, urlencode({
            'cas': '0',

        })), seed_value().to_bytes(8, byteorder='big', signed=False), **http_args)

        response = await http.get_async(url, **http_args)

    data = json.loads(response.content)[0]

    for _ in range(retries):
        value = int.from_bytes(b64decode(data['Value']), byteorder='big', signed=False)
        value += 1

        response = await http.put_async('{}?{}'.format(url, urlencode({
            'cas': str(data.get('ModifyIndex', data['CreateIndex'])),

        })), value.to_bytes(8, byteorder='big', signed=False), **http_args)

        if response.content.decode('utf-8').lower() == 'true':
            return value

        data = json.loads(await http.get_async(url, **http_args).content)[0]

    raise Exception('{}: consul failed'.format(url))


def nextval(*args, **kwargs):
    return run_coroutine(nextval_async(*args, **kwargs))
