import inspect
import re
from sppylibs.common.utils import symbol
from collections import defaultdict


VALUE_SIZE_LIMIT = 32


class EnumMeta(type):
    def __init__(cls, name, bases, nmspc):
        super(EnumMeta, cls).__init__(name, bases, nmspc)

        _enum_meta_impl(
            cls,
            orm_field_cls='django.db.models.CharField',
            form_field_cls='django.forms.ChoiceField',
        )


class IntEnumMeta(type):
    def __init__(cls, name, bases, nmspc):
        super(IntEnumMeta, cls).__init__(name, bases, nmspc)

        _enum_meta_impl(
            cls,
            orm_field_cls='django.db.models.IntegerField',
        )


def _enum_meta_impl(cls, *, orm_field_cls, form_field_cls=None):
    keys = []

    for member in inspect.getmembers(cls):
        if re.match(r'^[A-Z][A-Z0-9_]*$', member[0]) is None:
            continue

        if not isinstance(member[1], str) and not isinstance(member[1], int):
            raise Exception("Invalid class for Enum value: " + member[1].__class__.__name__)

        if len(str(member[1])) > VALUE_SIZE_LIMIT:
            raise Exception("Enum value is too big: " + str(member[1]))

        keys.append(member[0])

    # members sorted by alphabet
    keys = tuple(keys)

    if not hasattr(cls, 'all'):
        cls.all = lambda: keys

    if not hasattr(cls, 'values'):
        cls.values = lambda: [getattr(cls, k) for k in cls.all()]

    if not hasattr(cls, 'choices'):
        cls.choices = lambda: [(getattr(cls, k), k) for k in cls.all()]

    def populate_kwargs(dest, **source):
        for key, value in source.items():
            if key in dest:
                raise Exception("Tried to override default Enum parameter " + key + " for field")

            dest[key] = value

        return dest

    if not hasattr(cls, 'orm_field'):
        def __generate_orm_field(cls_, kwargs):
            return symbol(orm_field_cls)(**populate_kwargs(kwargs, max_length=VALUE_SIZE_LIMIT, choices=cls_.choices()))

        cls.orm_field = lambda **kwargs: __generate_orm_field(cls, kwargs)

    if form_field_cls is not None:
        if not hasattr(cls, 'form_field'):
            def __generate_form_field(cls_, kwargs):
                return symbol(form_field_cls)(**populate_kwargs(kwargs, choices=cls_.choices()))

            cls.form_field = lambda **kwargs: __generate_form_field(cls, kwargs)


class Enum:
    pass


class ManyToOneRemapEnum(Enum):
    @classmethod
    def make_remap(cls, data, *, from_method, to_method):
        reverse_data = defaultdict(set)

        for foreign_value, self_value in data.items():
            reverse_data[self_value].add(foreign_value)

        setattr(cls, from_method, lambda foreign_value: data.get(foreign_value))
        setattr(cls, to_method, lambda self_value: reverse_data.get(self_value, set()))


class OneToOneRemapEnum(Enum):
    @classmethod
    def make_remap(cls, data, *, from_method=None, to_method):
        reverse_data = {k: v for v, k in data.items()}

        if from_method is not None:
            setattr(cls, from_method, lambda foreign_value: reverse_data.get(foreign_value))

        setattr(cls, to_method, lambda self_value: data.get(self_value))
