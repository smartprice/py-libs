loop = None


def run_coroutine(coro):
    global loop

    import asyncio

    try:
        if loop is None:
            loop = asyncio.get_event_loop()
    except RuntimeError:
        pass

    if loop is None:
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

    future = asyncio.ensure_future(coro, loop=loop)
    loop.run_until_complete(future)

    return future.result()
