import requests
import json
import sys
import os
from sppylibs.common.enums.base import Enum, EnumMeta
from types import CoroutineType
from sppylibs.common.async_wrapper import run_coroutine
from urllib.parse import urlencode, quote, urlparse, parse_qs


DEFAULT_TIMEOUT = 3


class Methods(Enum, metaclass=EnumMeta):
    GET = 'get'
    POST = 'post'
    DELETE = 'delete'
    PUT = 'put'

    @staticmethod
    def call(method, *args, **kwargs): # for the sake of semantics
        return globals()[method](*args, **kwargs)


def __pool():
    session = requests.Session()

    for proto in ['http://', 'https://']:
        session.mount(proto, requests.adapters.HTTPAdapter(max_retries=2))

    return session


class Encoder(json.JSONEncoder):
    def default(self, obj):
        try:
            return super().default(obj)

        except:
            return repr(obj)


async def __request(method, *args, logger=None, cut_response=False, pool=None, **kwargs):
    if 'timeout' not in kwargs:
        kwargs['timeout'] = DEFAULT_TIMEOUT

    if pool is None:
        pool = __pool()

    reqid = os.environ.get('REQID', '')

    if logger is not None:
        logger.info('[{}] REQUEST: {}'.format(reqid, json.dumps({
            'args': [method] + list(args),
            'kwargs': kwargs,

        }, indent=4, sort_keys=True, cls=Encoder)))

    response = None
    exception = None

    try:
        response = getattr(pool, method)(*args, **kwargs)

    except:
        exception = sys.exc_info()[1]

    if isinstance(response, CoroutineType):
        response = await response

    if (response is not None) and (logger is not None):
        logger.info('[{}] RESPONSE [{}]: {}'.format(reqid, response.status_code, ('' if response.content is None else (response.content[:2048] if cut_response else response.content))))

    if exception is not None:
        raise exception

    return response


async def get_async(url, **kwargs):
    return await __request('get', url, **kwargs)


def get(*args, **kwargs):
    return run_coroutine(get_async(*args, **kwargs))


async def delete_async(url, **kwargs):
    return await __request('delete', url, **kwargs)


def delete(*args, **kwargs):
    return run_coroutine(delete_async(*args, **kwargs))


async def post_async(url, body, **kwargs):
    args = ['post', url]

    if body is not None:
        args.append(body)

    return await __request(*args, **kwargs)


def post(*args, **kwargs):
    return run_coroutine(post_async(*args, **kwargs))


async def put_async(url, body, **kwargs):
    args = ['put', url]

    if body is not None:
        args.append(body)

    return await __request(*args, **kwargs)


def put(*args, **kwargs):
    return run_coroutine(put_async(*args, **kwargs))


class AsyncPool(object):
    def __init__(self, *, timeout=DEFAULT_TIMEOUT, verify_ssl=False):
        import aiohttp

        self.__impl = aiohttp.ClientSession(
            conn_timeout=timeout,
            read_timeout=timeout,
            connector=aiohttp.TCPConnector(verify_ssl=verify_ssl),
        )

    async def post(self, url, body, **kwargs):
        kwargs['data'] = body

        return await self.__make_response(await self.__impl.post(url, **kwargs))

    async def get(self, url, **kwargs):
        return await self.__make_response(await self.__impl.get(url, **kwargs))

    async def delete(self, url, **kwargs):
        return await self.__make_response(await self.__impl.delete(url, **kwargs))

    async def put(self, url, body, **kwargs):
        kwargs['data'] = body

        return await self.__make_response(await self.__impl.put(url, **kwargs))

    async def __make_response(self, obj):
        out = requests.Response()

        out._content = await obj.content.read()
        out._content_consumed = True
        out.status_code = obj.status
        out.headers = obj.headers
        out.encoding = obj.charset

        return out

    async def close(self):
        await self.__impl.close()


class RawRequestBuilder(object):
    class QueryParamOpType(Enum, metaclass=EnumMeta):
        ADD = '+'
        REMOVE = '-'

    def __init__(
        self,
        *,
        method=None,
        content_type=None,
        url=None,
        body=None,
        proto='HTTP/1.1',
    ):
        self.__query_params = []
        self.__headers = []
        self.__method = None
        self.__url = None
        self.__body = None
        self.__proto = None

        for (value, method_) in [
            (method, 'set_method'),
            (url, 'set_url'),
            (body, 'set_body'),
            (proto, 'set_proto'),
        ]:
            if value is not None:
                getattr(self, method_)(value)

        if content_type is not None:
            self.add_header('Content-Type', content_type)

    def add_param(self, key, values):
        assert isinstance(values, (list, tuple, set, frozenset))

        self.__query_params.append((self.QueryParamOpType.ADD, key, values))

        return self

    def remove_param(self, key):
        self.__query_params.append((self.QueryParamOpType.REMOVE, key))

        return self

    def replace_param(self, key, values):
        self.remove_param(key)
        self.add_param(key, values)

        return self

    def add_header(self, key, value):
        self.__headers.append((key, value))

        return self

    def set_method(self, value):
        self.__method = value

        return self

    def set_url(self, value):
        self.__url = value

        return self

    def set_body(self, value):
        self.__body = value

        return self

    def set_proto(self, value):
        self.__proto = value

        return self

    def build(self):
        base = urlparse(self.__url)
        query_params = parse_qs(base.query)

        for op in self.__query_params:
            if op[0] == self.QueryParamOpType.ADD:
                if op[1] not in query_params:
                    query_params[op[1]] = []

                query_params[op[1]].extend(op[2])

            elif op[0] == self.QueryParamOpType.REMOVE:
                query_params.pop(op[1], None)

            else:
                assert False

        header_groups = [self.__headers]

        if base.netloc:
            header_groups.append([('Host', base.netloc)])

        body = self.__body

        if body is not None:
            if not isinstance(body, (bytes, memoryview)):
                body = body.encode('utf-8')

            header_groups.append([('Content-Length', str(len(body)))])

        path = '/'.join(map(quote, base.path.split('/')))

        if len(query_params) > 0:
            path += '?' + urlencode(query_params, doseq=True)

        parts = [
            '{} {} {}'.format(self.__method.upper(), path, self.__proto),
        ]

        for group in header_groups:
            for (key, value) in group:
                parts.append('{}: {}'.format(key, quote(value)))

        for _ in range(2):
            parts.append('')

        header = '\r\n'.join(parts).encode('utf-8')

        if body is None:
            return header

        else:
            return header + body
