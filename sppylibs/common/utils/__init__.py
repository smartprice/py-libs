def import_object(module_name, object_name=None):
    if object_name is None:
        last_dot_index = module_name.rindex('.')
        object_name = module_name[(last_dot_index + 1):]
        module_name = module_name[:last_dot_index]

    module = None

    try:
        module = __import__(module_name)

    except:
        pass

    for part in module_name.split('.')[1:]:
        module = getattr(module, part, None)

    return getattr(module, object_name, None)


def symbol(node):
    if isinstance(node, str):
        return import_object(node)

    return node
