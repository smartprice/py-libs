import os


NETCONF_SRV_FILE = os.getenv('NETCONF_SRV', os.path.join(os.sep, 'etc', 'netconf-srv.json'))
NETCONF_SRV = {}


if os.path.isfile(NETCONF_SRV_FILE):
    import json

    fh = open(NETCONF_SRV_FILE, 'r')
    NETCONF_SRV = json.loads(fh.read())
    fh.close()
